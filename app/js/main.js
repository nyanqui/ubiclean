$(document).ready(function() {
    /*  SCROLLTOP  MENU*/

    $('.menu__main--li').click(function() {
        flagScroll = false;
        $('.menu__main--li').removeClass("hover");
        $(this).addClass( "hover" );
        $(".menu-toogle").click();
        var hash = $("a", this).attr('href');
          $('html, body').stop().animate({
            scrollTop: $(hash).offset().top-30
          }, 1000, function(){
            flagScroll = true;
          });
          return false;
    });


    /*  MENU ACTIVE  */

    $('.navbar-collapse li a').on('click', function(e){
      $('li a.activo').removeClass('activo');
      $(this).addClass('activo');
      e.preventDefault();
    });



    /*  HEADER  */

    $(".menu-toogle").on("click", function () {
        $(".icon-bar").toggleClass("open");
        /*$(this).toggleClass("button__open");*/

        $("header").toggleClass("header--active");
        $("body, html").toggleClass("body-overflow");
        $("#bs-example-navbar-collapse-1").slideToggle(500);
    });

    //  HOME MINERA 

    $(".rueda__settings").on("click", function (e) {

      if (!$(this).next('.problemas__setting--menu').is(":visible")) {
        $('.problemas__setting--menu.settings__active').removeClass('settings__active');
      }
      $(this).next('.problemas__setting--menu').toggleClass("settings__active");
      e.preventDefault();    
    }); 

    /*    CAROUSEL SLIDER HOME    */
    $('.slider__home-carousel').owlCarousel({
      items: 1,
      loop: true,
      margin: 0,
      animateOut: 'fadeOut',
      autoplay: true
    });

    $('.slider__home-carousel').on('changed.owl.carousel', function(event) {
      $('.slider__eleccion--active').removeClass('slider__eleccion--active');
      $('.slider__electo').eq(event.page.index).addClass('slider__eleccion--active');
      $(".slider__descript-main:visible").fadeOut(500, function(){
        
        $(".slider__descript-main").eq(event.page.index).fadeIn(500);
      });
    });

    $('.slider__electo').click(function(e) {
        var index = $(this).data("index");

        $(".slider__home-carousel").trigger('to.owl.carousel', [index]);
        e.preventDefault();
    });

    var owl = $('.slider__home-carousel');
    owl.owlCarousel();
    // Go to the next item
    $('.customNextBtn').click(function() {
        owl.trigger('next.owl.carousel');
    })
    // Go to the previous item
    $('.customPrevBtn').click(function() {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl.carousel', [300]);
    })

    $(".__opcion").on("click", function(e){
    
        if($(window).width()) {
          var caja = $(this).data("box");

          $(".login-item-data").hide();
          $(".slider-home").hide();
          $(caja).show();
          $(".__opcion").removeClass("inversiones-active");
          $(this).addClass("inversiones-active");
          
        } 
        e.preventDefault();
    });
    
    $(window).on("load", function() {
        $(".twentytwenty-container").twentytwenty({
            no_overlay: true
        });
    });

    $(".btn-catgoria").on("click", function (e) {
        var cssClass = ".content-"+$(this).data("tarifa");

        $(".btn-catgoria").removeClass("btn-catgoria--active");
        $(this).addClass("btn-catgoria--active");

        $(".content-tarifas").hide();
        $(".content-tarifas"+cssClass).show();
        e.preventDefault();
    });
});
